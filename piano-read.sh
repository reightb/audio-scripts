#!/bin/env bash
# Dependencies:
#   audio-recorder bc wmctrl killall pkill playerctl pactl
# Send SIGHUP to script to self update i.e.
#   pkill -f ./piano-read.sh -HUP
# Thanks to:
# https://gist.github.com/niieani/29a054eb29d5306b32ecdfa4678cbb39

# Config # 
DEV_MODE=false
KEYBOARD="X6mini"
#KEYBOARD="DJControl Compact"
RECORDER="audio-recorder"

# Config end #

declare -i last_called=0
declare -i throttle_by=100 # ms

log() { echo "$@"; }
warn() { log "🚧 :: $@";}

log_execute() {
  log $1
  shift
  "$@"
}

last_modified_file_in_directory() {
  find $1 -printf '%T+ %p\n' | sort -r | head -n 1 | cut -d' ' -f2
}

traphup() {
    echo "** CAUGHT HUP -- RELAUNCHING **"
    $0 "$@" &
    exit 0
}
trap traphup HUP

@throttle() {
  local -i now=$(($(date +%s%N)/1000000))
  if (($now - $last_called >= $throttle_by))
  then
    "$@"
  fi
  last_called=$(($(date +%s%N)/1000000))
}

@throttle_by() {
    by=$1
    shift
    local -i now=$(($(date +%s%N)/1000000))
    if (($now - $last_called >= $by))
    then
        "$@"
    fi
    last_called=$(($(date +%s%N)/1000000))
}

@debounce() {
  if [[ ! -f ./executing ]]
  then
    touch ./executing
    "$@"
    ret_val=$?
    {
      sleep $throttle_by
      if [[ -f ./on-finish ]]
      then
        "$@"
        rm -f ./on-finish/
      fi
      rm -f ./executing
    } &
    return $ret_val
  elif [[ ! -f ./on-finish ]]
  then
    touch ./on-finish
  fi
}

log "🛠️  LAUNCHING PIANO-READ 🛠️"
aseqdump -p $KEYBOARD | \
  while IFS=" ," read src ev1 ev2 ch label1 data1 label2 data2 rest; do
    _script_restart() { pkill -f ./piano-read.sh -HUP; }
    _util_restart_plasmashell() { killall plasmashell; plasmashell & }
    _util_stop_heavy_stuff() { killall chrome; killall $RECORDER; pkill -f Pianoteq; }
    _util_stop_full() { killall chrome; killall $RECORDER; pkill -f Pianoteq ; killall dolphin-emu; }
    _audio_workspace() { log_execute "🔧 Starting audio workspace" pkill -f Pianoteq; killall $RECORDER; "$HOME/Desktop/x86-64bit/Pianoteq 7 STAGE" & $RECORDER & }
    _audio_record_start() { log_execute "🎤 Recording ($(date))" wmctrl -a audio; xdotool key ctrl+r; }
    _audio_record_stop() { wmctrl -a audio; xdotool key ctrl+x; log "🎤✅ Recording stop $(last_modified_file_in_directory $HOME/Audio)"; }
    _audio_global_mute() { pactl set-sink-volume @DEFAULT_SINK@ 0%; }
    _audio_global_player_toggle() { playerctl play-pause; }
    _audio_global_volume_set() {
      volume=$(echo "($data2 / 127.0 )*100.0" | bc -l) 
      echo "🔊 Setting volume to ${volume%.*}%"
      pactl -- set-sink-volume @DEFAULT_SINK@ "${volume%.*}%"
    }

    case "$label2" in 
      "velocity") ;; # ignore note keys
      *) 
        case "$data1" in
            "10") @throttle _audio_global_volume_set ;;
            "33") @throttle _audio_global_player_toggle ;;
            "34") @throttle_by 50 xdotool key Down ;;
            "35") @throttle_by 50 xdotool key Up ;;

            "74") # red keys
              case "$data2" in
                "06") @throttle _audio_record_start ;;
                "01") @throttle _audio_record_stop ;; 
                "37") @throttle _script_restart ;;
              esac
              ;;
            "68") @throttle _audio_record_stop ;;
            "67") @throttle _audio_workspace ;;

            "25") @throttle _util_stop_heavy_stuff ;; 
            "26") @throttle _util_restart_plasmashell ;;
            "27") @throttle _audio_global_mute ;;
            "28") @throttle _util_stop_full ;; 
        esac

        if $DEV_MODE; then @throttle_by 5 echo "--> $data1 $label2 $data2"; fi
    esac
done
